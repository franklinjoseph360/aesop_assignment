require('dotenv').config();
const express = require('express');
const compression = require('compression');
const routes = require('./routes/orders');

const app = express();

if (process.env.NODE_ENV === 'production') {
  app.use(compression());
}

app.use(express.json());

app.use('/', routes);

app.listen(process.env.PORT || 8080, () => {
  console.log(`App started at: ${process.env.PORT || 8080}`);
});

module.exports = app;
