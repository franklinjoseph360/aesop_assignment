/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

const should = chai.should();

chai.use(chaiHttp);

describe('Orders: Test => ', () => {
  describe('/orders', () => {
    it('should return all orders', (done) => {
      chai.request(server)
        .get('/orders')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.not.be.eql(null);
          done();
        });
    });
  });

  describe('/orders/carrier/:name', () => {
    it('should return have orders with only given carrier service name', (done) => {
      const name = 'Royal Mail';
      chai.request(server)
        .get(`/orders/carrier/${name}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.not.be.eql(null);
          done();
        });
    });
  });

  describe('/orders/qty/:barcode', () => {
    it('should return sum of quantities by sku barcode', (done) => {
      const barcode = 'ASK10';
      chai.request(server)
        .get(`/orders/qty/${barcode}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('qty');
          done();
        });
    });
  });
});
