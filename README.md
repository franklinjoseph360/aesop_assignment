# Aesop Coding Challenge

For the position of Full stack developer

## Authors

Franklin Joseph  
[LinkedIn](https://www.linkedin.com/in/franklin-joseph)

## Description

A simple Node js app that implements a Resful API to serve the following endpoints given the data provided in the Excel sheet.

* Get all orders `(/orders)`
* Get all orders by carrier name `(/orders/carrier/:name)`
* Get total sku quantity by sku barcode `(/orders/qty/:barcode)`

## Getting Started

### Dependencies

* Must have node and npm installed.
* Supports windows, mac and linux based systems.

### Setup or Clone Repository
* Run the following command in your terminal or command prompt:
```
> git clone https://franklinjoseph360@bitbucket.org/franklinjoseph360/aesop_assignment.git
```
* Add .env file and add the following or use the .env.sample (optional)
```
PORT = 8080
```

### Run the app
Run the following commands to start the app:
```
> npm install
> npm start `(for production)`
> npm run start-dev `(for development)`
```

Run the following command to run tests:
```
> npm test
```

## To see the results

* Get all orders
[/orders](http://localhost:8080/orders)
* Get all orders by carrier
[/orders/carrier/:name](http://localhost:8080/orders/carrier/Royal%20Mail)
* Get total sku units by sku barcode
[/orders/qty/:barcode](http://localhost:8080/orders/qty/B500HR14)


## Version History
* 1.0.0
    * Initial Release