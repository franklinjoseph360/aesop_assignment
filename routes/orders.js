const express = require('express');

const router = express.Router();

const ordersController = require('../controllers/orders');

router.get('/orders', ordersController.getAllOrders);
router.get('/orders/carrier/:name', ordersController.getOrdersByCarrierService);
router.get('/orders/qty/:barcode', ordersController.getQtyByBarCode);

module.exports = router;
