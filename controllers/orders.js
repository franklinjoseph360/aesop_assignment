const data = require('../data');

const getAllOrders = (req, res) => {
  try {
    res.json(data);
  } catch (err) {
    console.error(`Get All Orders error: ${err}`);
    res.status(400).send('Oops something went wrong. Please contact the API service provider');
  }
};

const getOrdersByCarrierService = (req, res) => {
  try {
    const carrierServiceName = req.params.name;
    const dataByCarrierService = data.filter((d) => d.carrierService === carrierServiceName);
    res.json(dataByCarrierService);
  } catch (err) {
    console.error(`Get Orders by Carrier service error: ${err}`);
    res.status(400).send('Oops something went wrong. Please contact the API service provider');
  }
};

const getQtyByBarCode = (req, res) => {
  try {
    let qty = 0;
    const skuBarCode = req.params.barcode;
    data.map((d) => {
      if (d.skuBarCode === skuBarCode) {
        qty += d.skuQuantity;
      }
      return null;
    });
    res.json({ qty });
  } catch (err) {
    console.error(`Get Total Quantity by barcode number error: ${err}`);
    res.status(400).send('Oops something went wrong. Please contact the API service provider');
  }
};

module.exports = { getAllOrders, getOrdersByCarrierService, getQtyByBarCode };
